/*******************************************************************************
* Include guard
*******************************************************************************/
#ifndef APP_BT_H_
#define APP_BT_H_

#include "wiced_bt_dev.h"
#include "wiced_bt_gatt.h"
#include <stdio.h>

#ifndef CYBSP_USER_LED
#define CYBSP_USER_LED P10_0
#endif

/* Typdef for function used to free allocated buffer to stack */
typedef void (*pfn_free_buffer_t)(uint8_t *);

/******************************************************************************
 *                                Constants
 ******************************************************************************/
extern uint16_t bt_conn_id;

extern int dataTempPos;
extern uint8_t* dataTemp;

extern uint16_t Vbat;

/****************************************************************************
 *                              FUNCTION DECLARATIONS
 ***************************************************************************/

/* Callback function for Bluetooth stack management type events */
wiced_bt_dev_status_t app_bt_management_callback    (wiced_bt_management_evt_t event, wiced_bt_management_evt_data_t *p_event_data);

/* Callback function for BLE scanning */
void BLEScanCallback			                    (wiced_bt_ble_scan_results_t *p_scan_result, uint8_t *p_adv_data);

/*Discovery functions*/
void startBTServiceDiscovery                        (void);
void startBTCharacteristicDiscovery                 (void);
void startBTDescriptorDiscovery                     (void);

/* Helper functions to allocate/free buffers for GATT operations */
uint8_t *app_bt_alloc_buffer                        (uint16_t len);
void app_bt_free_buffer                             (uint8_t *p_data);

void writeAttribute                                 (uint16_t bt_conn_id, uint16_t handle, uint16_t offset, wiced_bt_gatt_auth_req_t auth_req, uint16_t len, uint8_t* val );

#endif /* APP_BT_H_ */


/* [] END OF FILE */
