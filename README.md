# SAE ESE Wifi BLE - Central

Code pour la carte centrale qui communique avec les périphériques via Bluetooth et transmet les données à la plateforme IoT via HTTP.

* Exemple communication LED+bouton avec **autre carte PSoC 6** : branche *master* 
* Exemple communication avec **module solaire MPPT** : branche *communication_MPPT*

### Matériel et logiciel requis
- Kit de prototypage PSoC : CY8CPROTO-062-4343W
- ModusToolbox 3.1

### Guide d'installation

1. Télécharger le dossier **central** à l'emplacement souhaité.
2. Ouvrir Eclipse IDE for ModusToolbox et importer le projet en cliquant sur *Import existing application in place* dans le panneau en bas à gauche de l'application (Quick panel).
3. Connecter la carte PSoC via USB.
4. Modifier les identifiants nécessaires à la connexion wifi et HTTP dans le fichier 'http_client.h' : **WIFI_SSID**, **WIFI_PASSWORD**, **WIFI_SECURITY_TYPE**, **HTTP_PORT**, **HTTP_SERVER_HOST**, **HTTP_PATH**.
5. Modifier le nom de l'appareil périphérique dans 'app_bt.c' : **SEARCH_DEVICE_NAME**.
6. Lancer la compilation du code et la programmation de la carte en appuyant sur le bouton *Run* (si une boîte de dialogue apparaît, choisir *Program*). Si la programmation échoue, assurez-vous que le programmateur de la carte soit à jour : ouvrir l'application **Cypress Programmer**. À l'ouverture, l'application devrait avertir de l'obsolescence du programmateur. Cliquer sur le bouton *Upgrade Firmware* dans la boîte de dialogue qui apparaît.
7. Une communication UART est nécessaire pour vérifier le statut de la connexion bluetooth et de la connexion HTTP (par exemple via TeraTerm). Baud = 115200, flow control = Xon/Xoff. Au démarrage, la centrale se connecte au réseau wifi, et s'il est accessible, au serveur HTTP de la plateforme Thingsboard.
8. La connexion au périphérique bluetooth doit se faire manuellement : lancer le scan d'appareils en envoyant 's' sur l'interface série. L'appairage devrait s'effectuer automatiquement.
9. Maintenant que le périphérique bluetooth est connecté, effectuer ces commandes dans l'ordre :
   1. Activer le suivi de notifications en envoyant 'n' sur l'interface série.
   2. Effectuer une demande de relevé de données en envoyant 'r' sur l'interface série. Une série de notifications devrait s'afficher sur l'interface.
   3. Envoyer une requête HTTP 'POST' contenant la dernière valeur relevée de la tension de la batterie sur le module MPPT. 

### Travail à réaliser

- Gérer la connexion bluetooth de plusieurs périphériques en plus du MPPT.
- Gérer la transmission de la lecture de donnée provenant du capteur associé au périphérique (bouton d'abonnement + remontée des données notifiées).
- Remonter d'autres données que la tension batterie.

### Pistes d'amélioration 

- Automatiser la connexion bluetooth et la découverte de services/caractéristiques au travers d'une machine à états.
- Utiliser un autre protocole pour remonter les données au serveur (MQTT).
